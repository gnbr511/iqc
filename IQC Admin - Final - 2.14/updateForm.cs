﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class lotInformationUpdateForm : Form
    {
        public string lotInfoId, invoiceNo, partNo, partName, totalQty, 
            quantityReceived, lotNo, lotQty, boxNumber, reject, sampleSize, 
            goodsCode, materialCodeNo, remarks, date1, kenId, diff, totalGood;

        public string lotInformationFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;

        string query;
        bool mouseDown;
        private Point offset;
        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }    

        public lotInformationUpdateForm()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialBoxNoTextBox.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                {
                    try
                    {
                        query = "UPDATE LotNumber " +
                       "SET part_no = '" + partNoTextBox.Text +
                       "', part_name = '" + partNameTextBox.Text +
                       "', total_quantity = '" + totalQTYTextBox.Text +
                       "', quantity_recieved = '" + quantityReceivedTextBox.Text +
                       "', lot_no = '" + lotNoTextBox.Text +
                       "', lot_quantity = '" + lotQuantityTextBox.Text +
                       "', box_number = '" + boxNoTextBox.Text +
                       "', reject = '" + rejectTextBox.Text +
                       "', sample_size = '" + sampleSizeTextBox.Text +
                       "', goodsCode = '" + goodsCodeTextBox.Text +
                       "', remarks = '" + remarksTextBox.Text +
                       "', Date = '" + lotNumberDateTimePicker.Text +
                       "', KEN_ID = '" + kenIDTextBox.Text +
                       "', DIFF = '" + diffTextBox.Text +
                       "', Total_good = '" + totalGoodTextBox.Text +
                       "' WHERE ID = '" + lotInfoIdTextBox.Text + "'";
                        SqlCommand command = new SqlCommand(query, DBHelper.connection);
                        command.ExecuteNonQuery();
                        frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                        obj.comboBox2_SelectedIndexChanged(sender, e);
                        MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }     

        private void updateForm_Load(object sender, EventArgs e)
        {
            //this.Location = new Point(400, 255);

            if(lotInformationFormLabel == "update")
            {
                lotInfoFormLabel30.Text = "LOT INFORMATION UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                lotInfoIdTextBox.Text = lotInfoId;
                invoiceNoTextBox.Text = invoiceNo;
                partNoTextBox.Text = partNo;
                partNameTextBox.Text = partName;
                totalQTYTextBox.Text = totalQty;
                quantityReceivedTextBox.Text = quantityReceived;
                lotNoTextBox.Text = lotNo;
                lotQuantityTextBox.Text = lotQty;
                boxNoTextBox.Text = boxNumber;
                rejectTextBox.Text = reject;
                sampleSizeTextBox.Text = sampleSize;
                goodsCodeTextBox.Text = goodsCode;
                materialBoxNoTextBox.Text = materialCodeNo;
                remarksTextBox.Text = remarks;
                lotNumberDateTimePicker.Text = date1;
                kenIDTextBox.Text = kenId;
                diffTextBox.Text = diff;
                totalGoodTextBox.Text = totalGood;
            }
            else
            {
                lotInfoFormLabel30.Text = lotInformationFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceNoTextBox.Text = comboBox1InvoiceNo;
                materialBoxNoTextBox.Text = comboBox2PartNo;
            }
            
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
