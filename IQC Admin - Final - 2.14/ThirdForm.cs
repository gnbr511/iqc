﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class ThirdForm : Form
    {
        public ThirdForm()
        {
            InitializeComponent();
        }
        SqlDataAdapter dataAdapter1, dataAdapter2, dataAdapter3, dataAdapter4, dataAdapter5;
        SqlCommandBuilder commandBuilder1, commandBuilder2, commandBuilder3, commandBuilder4, commandBuilder5;
        DataTable dt5, dt4, dt3, dt2, dt1;

        private void ThirdForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode.ToString() == "D")
            {
                if (comboBox1.Text != "" && comboBox2.Text != "")
                {
                    var option = MessageBox.Show("DELETE ENTIRE DATA OF " + comboBox2.Text + "?", "IQC System - Admin", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (option == DialogResult.OK)
                    {
                        DBHelper.deleteEntireData("LotNumber", comboBox1.Text, comboBox2.Text);
                        DBHelper.deleteEntireData("inspectiondata", comboBox1.Text, comboBox2.Text);
                        DBHelper.deleteEntireData("Appearance_Inspection", comboBox1.Text, comboBox2.Text);
                        DBHelper.deleteEntireData("DimensionalCheck", comboBox1.Text, comboBox2.Text);
                        DBHelper.deleteEntireData("FunctionalCheck", comboBox1.Text, comboBox2.Text);

                        MessageBox.Show("DATA OF "+comboBox2.Text+" WAS DELETED SUCCESSFULLY!", "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("NO DATA TO DELETE, GENERATE DATA FIRST!", "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            if (e.Control && e.KeyCode.ToString() == "S")
            {
                try
                {
                    comboBox1.Focus();

                    commandBuilder1 = new SqlCommandBuilder(dataAdapter1);
                    dataAdapter1.Update(dt1);

                    commandBuilder2 = new SqlCommandBuilder(dataAdapter2);
                    dataAdapter2.Update(dt2);

                    commandBuilder3 = new SqlCommandBuilder(dataAdapter3);
                    dataAdapter3.Update(dt3);

                    commandBuilder4 = new SqlCommandBuilder(dataAdapter4);
                    dataAdapter4.Update(dt4);

                    commandBuilder5 = new SqlCommandBuilder(dataAdapter5);
                    dataAdapter5.Update(dt5);

                    MessageBox.Show("DATA ADDED / UPDATED!", "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        private void ThirdForm_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.AutoCompleteCustomSource.Clear();
            comboBox1.Items.AddRange(DBHelper.ComboBoxItems("LotNumber", "invoice_no", 0).ToArray());
            comboBox1.AutoCompleteCustomSource.AddRange(DBHelper.ComboBoxItems("LotNumber", "invoice_no", 0).ToArray());

            MessageBox.Show("NOTE:\n\n"+
            "WHEN EDITING:\n"+
            "- Do not change the id, invoice_no, MaterialCodeBoxSeqID.\n\n"+
            "WHEN ADDING:\n"+
            "- Copy the invoice & MaterialCodeBoxSeqID.\n"+
            "- Leave id blank.\n\n"+
            "WHEN DELETING:\n"+
            "- Make sure that the selected invoice and part no. is correct.\n"+
            "- System can't undo changes",
            "IQC System - Admin", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            comboBox2.AutoCompleteCustomSource.Clear();
            comboBox2.Text = "";
            comboBox2.Items.AddRange(DBHelper.ComboBoxItems("LotNumber WHERE invoice_no = '" + comboBox1.Text + "'", "MaterialCodeBoxSeqID", 0).ToArray());
            comboBox2.AutoCompleteCustomSource.AddRange(DBHelper.ComboBoxItems("LotNumber WHERE invoice_no = '" + comboBox1.Text + "'", "MaterialCodeBoxSeqID", 0).ToArray());
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataAdapter1 = new SqlDataAdapter("SELECT * FROM LotNumber WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '"+comboBox2.Text+"'", DBHelper.connection);
            dt1 = new DataTable();
            dataAdapter1.Fill(dt1);
            dataGridView1.DataSource = dt1;

            dataAdapter2 = new SqlDataAdapter("SELECT * FROM inspectiondata WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
            dt2 = new DataTable();
            dataAdapter2.Fill(dt2);
            dataGridView2.DataSource = dt2;

            dataAdapter3 = new SqlDataAdapter("SELECT * FROM DimensionalCheck WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
            dt3 = new DataTable();
            dataAdapter3.Fill(dt3);
            dataGridView3.DataSource = dt3;

            dataAdapter4 = new SqlDataAdapter("SELECT * FROM Appearance_Inspection WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
            dt4 = new DataTable();
            dataAdapter4.Fill(dt4);
            dataGridView4.DataSource = dt4;

            dataAdapter5 = new SqlDataAdapter("SELECT * FROM FunctionalCheck WHERE invoice_no = '" + comboBox1.Text + "' AND MaterialCodeBoxSeqID = '" + comboBox2.Text + "'", DBHelper.connection);
            dt5 = new DataTable();
            dataAdapter5.Fill(dt5);
            dataGridView5.DataSource = dt5;
        }
        private void dataGridView1_Click(object sender, EventArgs e)
        {
            
        }
        private void dataGridView1_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void dataGridView2_KeyUp(object sender, KeyEventArgs e)
        {
        
        }
        private void dataGridView3_KeyUp(object sender, KeyEventArgs e)
        {
                    
        }
        private void dataGridView4_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void dataGridView5_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
