﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IQCAdmin
{
    public partial class SecondForm : Form
    {
        public SecondForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SecondForm_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.AutoCompleteCustomSource.Clear();
            comboBox1.Items.AddRange(DBHelper.ComboBoxItems("LotNumber", "invoice_no", 0).ToArray());
            comboBox1.AutoCompleteCustomSource.AddRange(DBHelper.ComboBoxItems("LotNumber", "invoice_no", 0).ToArray());
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.sP_SummaryReport_LotNo2TableAdapter.Fill(this.iQCDatabaseDataSet.SP_SummaryReport_LotNo2, comboBox1.Text);

            this.reportViewer1.RefreshReport();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void reportViewer1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void SecondForm_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
