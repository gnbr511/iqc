﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class UpdateAppearanceInspectionForm : Form
    {
        public string id, invoice_no, ig_checkpoints, instrument_used, result, judgement, 
            remarks, goodsCode, defectQTY, defect_enc, MaterialCodeBoxSeqID;

        public string appearanceInspectionFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;
        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialCodeBoxTextBox.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                { 
                    query = "UPDATE Appearance_Inspection " + 
                        "SET ig_checkpoints = '" + igCheckPointsTextBox.Text +
                        "', instrument_used = '" + instrumentUsedTextBox.Text +
                        "', result = '" + resultTextBox.Text +
                        "', remarks = '" + remarksTextBox.Text +
                        "', goodsCode = '" + goodsCodeTextBox.Text +
                        "', defectqty = '" + defectQtyTextBox.Text +
                        "', defect_enc = '" + defectEncTextBox.Text +
                        "', MaterialCodeBoxSeqID = '" + materialCodeBoxTextBox.Text +
                        "' WHERE id = '" + idTextBox1.Text + "'";
                    SqlCommand command = new SqlCommand(query, DBHelper.connection);
                    command.ExecuteNonQuery();
                    frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                    obj.comboBox2_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateAppearanceInspectionForm_Load(object sender, EventArgs e)
        {
            if(appearanceInspectionFormLabel == "update")
            {
                appearanceInspectionlabel30.Text = "APPEARANCE INSPECTION UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                idTextBox1.Text = id;
                invoiceTextBox2.Text = invoice_no;
                igCheckPointsTextBox.Text = ig_checkpoints;
                instrumentUsedTextBox.Text = instrument_used;
                resultTextBox.Text = result;
                judgementTextBox.Text = judgement;
                remarksTextBox.Text = remarks;
                goodsCodeTextBox.Text = goodsCode;
                defectQtyTextBox.Text = defectQTY;
                defectEncTextBox.Text = defect_enc;
                materialCodeBoxTextBox.Text = MaterialCodeBoxSeqID;
            }
            else
            {
                appearanceInspectionlabel30.Text = appearanceInspectionFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceTextBox2.Text = comboBox1InvoiceNo;
                materialCodeBoxTextBox.Text = comboBox2PartNo;
            }           
        }

        string query;
        bool mouseDown;
        private Point offset;
        public UpdateAppearanceInspectionForm()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }
    }
}
