﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class dimensionalCheckUpdateForm : Form
    {
        public string id, invoiceNO, checkpoints, instrumentUsed, sampleUnit, sample1, sample2,
            sample3, sample4, sample5, sample6, sample7, sample8, sample9, sample10, minimum,
            average, maximum, lowerSpecLimit, upperSpecLimit, judgement, remarks, goodsCode, defectQty,
            defectEnc, materialCodeBox, date1, remarksCheckPoint;

        private void addButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show("");
        }

        public string dimesionalCheckFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;

        string query;
        bool mouseDown;
        private Point offset;
        public dimensionalCheckUpdateForm()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DimensionalCheckUpdateForm_Load(object sender, EventArgs e)
        {
            //this.Location = new Point(425, 273);
            if(dimesionalCheckFormLabel == "update")
            {                
                dimensionalCheckFormlabel30.Text = "DIMENSIONAL CHECK UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                idTextBox.Text = id;
                invoiceNotextBox.Text = invoiceNO;
                checkPointTextBox1.Text = checkpoints;
                instrumentUsedTextBox2.Text = instrumentUsed;
                sampleUnitTextBox3.Text = sampleUnit;
                sample1TextBox4.Text = sample1;
                sample2TextBox5.Text = sample2;
                sample3TextBox6.Text = sample3;
                sample4TextBox7.Text = sample4;
                sample5TextBox8.Text = sample5;
                sample6TextBox9.Text = sample6;
                sample7TextBox10.Text = sample7;
                sample8TextBox11.Text = sample8;
                sample9TextBox12.Text = sample9;
                sample10TextBox13.Text = sample10;
                minimumTextBox14.Text = minimum;
                averageTextBox15.Text = average;
                maximumTextBox16.Text = maximum;
                LowerSpecLimitTextBox17.Text = lowerSpecLimit;
                upperSpecLimitTextBox18.Text = upperSpecLimit;
                judgementTextBox20.Text = judgement;
                remarksTextBox21.Text = remarks;
                goodsCodeTextBox22.Text = goodsCode;
                defectQtyTextBox23.Text = defectQty;
                defectEncTextBox24.Text = defectEnc;
                materialBoxTextBox25.Text = materialCodeBox;
                dateTimePicker1.Text = date1;
                remarksCheckPointsTextBox1.Text = remarksCheckPoint;
            }
            else
            {
                dimensionalCheckFormlabel30.Text = dimesionalCheckFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceNotextBox.Text = comboBox1InvoiceNo;
                materialBoxTextBox25.Text = comboBox2PartNo;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialBoxTextBox25.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                {
                    query = "UPDATE DimensionalCheck " +
                        "SET checkpoints = '" + checkPointTextBox1.Text +
                        "', instrument_used = '" + instrumentUsedTextBox2.Text +
                        "', sample_unit = '" + sampleUnitTextBox3.Text +
                        "', sample1 = '" + sample1TextBox4.Text +
                        "', sample2 = '" + sample2TextBox5.Text +
                        "', sample3 = '" + sample3TextBox6.Text +
                        "', sample4 = '" + sample4TextBox7.Text +
                        "', sample5 = '" + sample5TextBox8.Text +
                        "', sample6 = '" + sample6TextBox9.Text +
                        "', sample7 = '" + sample7TextBox10.Text +
                        "', sample8 = '" + sample8TextBox11.Text +
                        "', sample9 = '" + sample9TextBox12.Text +
                        "', sample10 = '" + sample10TextBox13.Text +
                        "', minimum = '" + minimumTextBox14.Text +
                        "', average = '" + averageTextBox15.Text +
                        "', maximum = '" + maximumTextBox16.Text +
                        "', lower_spec_limit = '" + LowerSpecLimitTextBox17.Text +
                        "', upper_spec_limit = '" + upperSpecLimitTextBox18.Text +
                        "', judgement = '" + judgementTextBox20.Text +
                        "', remarks = '" + remarksTextBox21.Text +
                        "', goodsCode = '" + goodsCodeTextBox22.Text +
                        "', defectqty = '" + defectQtyTextBox23.Text +
                        "', defect_enc = '" + defectEncTextBox24.Text +
                        "', Date = '" + dateTimePicker1.Text +
                        "', Remarks_chckpoint = '" + remarksCheckPointsTextBox1.Text +
                        "' WHERE id = '" + idTextBox.Text + "'";
                    SqlCommand command = new SqlCommand(query, DBHelper.connection);
                    command.ExecuteNonQuery();
                    frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                    obj.comboBox2_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
