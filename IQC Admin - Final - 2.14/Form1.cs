﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using ClosedXML.Excel;

namespace IQCAdmin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(DBHelper.connection.State == ConnectionState.Closed)
            {
                DBHelper.connection.Open();
            }
        }
        #region DesigningForm
        private int tolerance = 12;
        private const int WM_NCHITTEST = 132;
        private const int HTBOTTOMRIGHT = 17;
        private Rectangle sizeGripRectangle;
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_NCHITTEST:
                    base.WndProc(ref m);
                    var hitPoint = this.PointToClient(new Point(m.LParam.ToInt32() & 0xffff, m.LParam.ToInt32() >> 16));
                    if (sizeGripRectangle.Contains(hitPoint))
                        m.Result = new IntPtr(HTBOTTOMRIGHT);
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }
        //Main panel sizing
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            var region = new Region(new Rectangle(0, 0, this.ClientRectangle.Width, this.ClientRectangle.Height));
            sizeGripRectangle = new Rectangle(this.ClientRectangle.Width - tolerance, this.ClientRectangle.Height - tolerance, tolerance, tolerance);
            region.Exclude(sizeGripRectangle);
            this.bgPanel.Region = region;
            this.Invalidate();
        }
        //SizingGrip
        protected override void OnPaint(PaintEventArgs e)
        {
            SolidBrush blueBrush = new SolidBrush(Color.FromArgb(244, 244, 244));
            e.Graphics.FillRectangle(blueBrush, sizeGripRectangle);
            base.OnPaint(e);
            ControlPaint.DrawSizeGrip(e.Graphics, Color.Transparent, sizeGripRectangle);
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        int lx, ly;
        int sw, sh;
        private void btn_maximize_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;

            btn_maximize.Visible = false;
            btn_normal.Visible = true;

            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
        }
        private void btn_normal_Click(object sender, EventArgs e)
        {
            btn_maximize.Visible = true;
            btn_normal.Visible = false;
            this.Size = new Size(sw, sh);
            this.Location = new Point(lx, ly);
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        private void btn_minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panelTitle_MouseMove(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        #endregion

        //for Main panel
        private void CallingForms<formname>() where formname : Form, new()
        {
            Form forms;
            forms = panelMain.Controls.OfType<formname>().FirstOrDefault();

            if (forms == null)
            {
                forms = new formname();
                forms.TopLevel = false;
                forms.FormBorderStyle = FormBorderStyle.None;//pede icomment for different approach
                forms.Dock = DockStyle.Fill;//pede icomment for different approach
                forms.WindowState = FormWindowState.Maximized;
                panelMain.Controls.Add(forms);
                panelMain.Tag = forms;
                forms.Show();
                forms.BringToFront();
                forms.FormClosed += new FormClosedEventHandler(CloseForms);
            }

            else
            {
                forms.BringToFront();
            }
        }
        private void first_button_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(60, 83, 147);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);

            if (Application.OpenForms.OfType<frmEdit_Data>().Any())
            {
                Application.OpenForms["frmEdit_Data"].Close();
            }
            if (Application.OpenForms.OfType<SecondForm>().Any())
            {
                Application.OpenForms["SecondForm"].Close();
            }
            password.Visible = false;
            CallingForms<FirstForm>();
        }
        private void second_button_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(60, 83, 147);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);

            if (Application.OpenForms.OfType<frmEdit_Data>().Any())
            {
                Application.OpenForms["frmEdit_Data"].Close();
            }
            if (Application.OpenForms.OfType<FirstForm>().Any())
            {
                Application.OpenForms["FirstForm"].Close();
            }
            password.Visible = false;
            CallingForms<SecondForm>();           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(60, 83, 147);

            if (Application.OpenForms.OfType<FirstForm>().Any())
            {
                Application.OpenForms["FirstForm"].Close();
            }
            if (Application.OpenForms.OfType<SecondForm>().Any())
            {
                Application.OpenForms["SecondForm"].Close();
            }            
            password.Visible = true;
            password_textBox.Focus();            
        }
        private void password_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Enter)
            {
                if(password_textBox.Text == "iqcadmin")
                {
                    password_textBox.Text = "";
                    password.Visible = false;
                    CallingForms<frmEdit_Data>();
                    //button1.BackColor = Color.FromArgb(12, 61, 92);
                }
                else
                {
                    var opt = MessageBox.Show("Incorrect password!", "IQC Admin", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                    if(opt == DialogResult.Retry)
                    {
                        password_textBox.Text = "";
                    }
                    else
                    {
                        password_textBox.Text = "";
                        password.Visible = false;
                    }
                }
            }
        }

        private void powerBi_Click(object sender, EventArgs e)
        {
            password.Visible = false;
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(204, 205, 207);
            powerBi.BackColor = Color.FromArgb(60, 83, 147);
            editButton.BackColor = Color.FromArgb(204, 205, 207);

            ProcessStartInfo sInfo1 = new ProcessStartInfo("https://app.powerbi.com/view?r=eyJrIjoiOGUxNzExN2UtM2FmMS00NjFlLTgwZTItZjZkM2I0OTBjOGE3IiwidCI6IjUxMTFmMDI3LTVkNzYtNDcxNy1hY2ZmLTVlNzgyOTViODgzMSIsImMiOjEwfQ%3D%3D&pageName=ReportSection");
            Process.Start(sInfo1);            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if ((DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString()+":"+DateTime.Now.Second.ToString()) == "13:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "15:1:0" ||
            //   (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "17:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString()) == "10:1:0" || 
            //   (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "22:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "1:1:0" ||
            //   (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString()) == "3:1:0" || (DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString()) == "5:1:0")
            //{
            //    ProcessStartInfo sInfo2 = new ProcessStartInfo("https://app.powerbi.com/view?r=eyJrIjoiOGUxNzExN2UtM2FmMS00NjFlLTgwZTItZjZkM2I0OTBjOGE3IiwidCI6IjUxMTFmMDI3LTVkNzYtNDcxNy1hY2ZmLTVlNzgyOTViODgzMSIsImMiOjEwfQ%3D%3D&pageName=ReportSection");
            //    Process.Start(sInfo2);
            //}
        }

        private void panelMain_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void password_Enter(object sender, EventArgs e)
        {

        }

        private void password_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void panelMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelTitle_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bgPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panelMain_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            RDLCReportPracticeForm rdlc = new RDLCReportPracticeForm();
            rdlc.ShowDialog();
        }

        private void third_button_Click(object sender, EventArgs e)
        {
            password.Visible = false;
            //these codes switches the colors of the buttons when clicked.
            first_button.BackColor = Color.FromArgb(204, 205, 207);
            second_button.BackColor = Color.FromArgb(204, 205, 207);
            third_button.BackColor = Color.FromArgb(60, 83, 147);
            powerBi.BackColor = Color.FromArgb(204, 205, 207);
            editButton.BackColor = Color.FromArgb(204, 205, 207);

            //Exporting to Excel
            string folderPath = @"\\192.168.1.11\Published Installers\IQC - Windows\IQC Admin (Resources)\References\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(DBHelper.exportData("inspectiondata"), "InspectionData");
                wb.Worksheets.Add(DBHelper.exportData("DimensionalCheck"), "DimensionalCheck");
                wb.Worksheets.Add(DBHelper.exportData("Appearance_Inspection"), "Appearance_Inspection");
                wb.Worksheets.Add(DBHelper.exportData("FunctionalCheck"), "FunctionalCheck");
                wb.Worksheets.Add(DBHelper.exportData("tblOverall_Judgement"), "OverallJudgement");
                wb.SaveAs(folderPath + "Data.xlsx");
            }
            ProcessStartInfo sInfo2 = new ProcessStartInfo("file://192.168.1.11/Published%20Installers/IQC%20-%20Windows/IQC%20Admin%20(Resources)/DailyReport_v3.0.xlsx");
            Process.Start(sInfo2);            
        }
        private void CloseForms(object sender, FormClosedEventArgs e)
        {
            //if(Application.OpenForms["FirstForm"] == null)
            //{
            //    first_button.BackColor = Color.FromArgb(4, 41, 68);
            //}
            //if (Application.OpenForms["SecondForm"] == null)
            //{
            //    second_button.BackColor = Color.FromArgb(4, 41, 68);
            //}
            //if (Application.OpenForms["ThirdForm"] == null)
            //{
            //    button1.BackColor = Color.FromArgb(4, 41, 68);
            //}
        }
    }
}
