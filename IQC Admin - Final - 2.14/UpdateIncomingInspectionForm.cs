﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class UpdateIncomingInspectionForm : Form
    {
        public string id, invoiceNo, approved, checked1, prepared, approvedDate, checkedDate, preparedDate, temperature,
            assemblyLine, partNo, rohsCompliance, humidity, inspectedDate, receivedDate, supplier, maker, inspector, materialType, productionType,
            inspectionType, oir, testReport, sampleSize, ulMarking, coc, partname, invoiceQty, goodsCode, materialCodeBoxSeqID, date1;

        public string incomingInspectionFormLabel = "update";
        public string comboBox1InvoiceNo, comboBox2PartNo;

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if(mouseDown == true)
            {
                Point currentScreenPosition = PointToScreen(e.Location);
                Location = new Point(currentScreenPosition.X - offset.X, currentScreenPosition.Y - offset.Y);
            }
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            offset.X = e.X;
            offset.Y = e.Y;
            mouseDown = true;
        }

        string query;
        bool mouseDown;
        private Point offset;
        public UpdateIncomingInspectionForm()
        {
            InitializeComponent();
        }
        private void saveButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "Click 'OK' to confirm the update to " + materialBoxTextBox25.Text + ".";
                DialogResult messageResult = MessageBox.Show(message, "Please Confirm!", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
                if (messageResult == DialogResult.OK)
                {
                    query = "UPDATE inspectiondata " +
                        "SET approved = '" + approvedtextBox.Text +
                        "', checked = '" + checkedTextBox.Text +
                        "', prepared = '" + preparedTextBox.Text +
                        "', approved_date = '" + approveDatedateTimePicker1.Text +
                        "', checked_date = '" + checkedDatedateTimePicker2.Text +
                        "', prepared_date = '" + preparedDatedateTimePicker3.Text +
                        "', temperature = '" + temperatureTextBox.Text +
                        "', assembly_line = '" + assemblyLineTextBox.Text +
                        "', part_number = '" + partNoTextBox.Text +
                        "', rohs_compliance = '" + rohsComplianceTextBox.Text +
                        "', humidity = '" + humidityTextBox.Text +
                        "', inspected_date = '" + preparedDatedateTimePicker3.Text +
                        "', recieved_date = '" + receivedDatedateTimePicker5.Text +
                        "', supplier = '" + suppliertextBox17.Text +
                        "', maker = '" + makertextBox16.Text +
                        "', inspector = '" + inspectortextBox15.Text +
                        "', material_type = '" + materialTypetextBox14.Text +
                        "', production_type = '" + productionTypetextBox13.Text +
                        "', inspection_type = '" + inspectionTypetextBox12.Text +
                        "', oir = '" + oirTextBox11.Text +
                        "', test_report = '" + testReporttextBox32.Text +
                        "', sample_size = '" + sampleSizetextBox31.Text +
                        "', ul_marking = '" + ulMarkingtextBox30.Text +
                        "', coc = '" + coctextBox29.Text +
                        "', partname = '" + partNametextBox28.Text +
                        "', invoicequant = '" + invoiceQTYtextBox27.Text +
                        "', goodsCode = '" + goodsCodetextBox26.Text +
                        "', Date = '" + IncInspecDateTimePicker.Text +
                        "' WHERE id = '" + incInspecIdTextBox.Text + "'";                    
                    SqlCommand command = new SqlCommand(query, DBHelper.connection);
                    command.ExecuteNonQuery();
                    frmEdit_Data obj = (frmEdit_Data)Application.OpenForms["frmEdit_Data"];
                    obj.comboBox2_SelectedIndexChanged(sender, e);
                    MessageBox.Show("Data has been updated successfully.", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }       

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateIncomingInspectionForm_Load(object sender, EventArgs e)
        {
            //this.Location = new Point(350, 255);

            if (incomingInspectionFormLabel == "update")
            {
                incomingInspectionFormlabel30.Text = "INCOMING INSPECTION UPDATE";
                addButton.Visible = false;
                saveButton.Visible = true;

                this.AcceptButton = saveButton;

                incInspecIdTextBox.Text = id;
                invoiceNotextBox.Text = invoiceNo;
                approvedtextBox.Text = approved;
                checkedTextBox.Text = checked1;
                preparedTextBox.Text = prepared;
                approveDatedateTimePicker1.Text = approvedDate;
                checkedDatedateTimePicker2.Text = checkedDate;
                preparedDatedateTimePicker3.Text = preparedDate;
                temperatureTextBox.Text = temperature;
                assemblyLineTextBox.Text = assemblyLine;
                partNoTextBox.Text = partNo;
                rohsComplianceTextBox.Text = rohsCompliance;
                humidityTextBox.Text = humidity;
                inspectedDatedateTimePicker4.Text = inspectedDate;
                receivedDatedateTimePicker5.Text = receivedDate;
                suppliertextBox17.Text = supplier;
                makertextBox16.Text = maker;
                inspectortextBox15.Text = inspector;
                materialTypetextBox14.Text = materialType;
                productionTypetextBox13.Text = productionType;
                inspectionTypetextBox12.Text = inspectionType;
                oirTextBox11.Text = oir;
                testReporttextBox32.Text = testReport;
                sampleSizetextBox31.Text = sampleSize;
                ulMarkingtextBox30.Text = ulMarking;
                coctextBox29.Text = coc;
                partNametextBox28.Text = partname;
                invoiceQTYtextBox27.Text = invoiceQty;
                goodsCodetextBox26.Text = goodsCode;
                materialBoxTextBox25.Text = materialCodeBoxSeqID;
                IncInspecDateTimePicker.Text = date1;
            }
            else
            {
                incomingInspectionFormlabel30.Text = incomingInspectionFormLabel;
                addButton.Visible = true;
                saveButton.Visible = false;

                this.AcceptButton = addButton;

                invoiceNotextBox.Text = comboBox1InvoiceNo;
                materialBoxTextBox25.Text = comboBox2PartNo;
            }
        }
    }
}
