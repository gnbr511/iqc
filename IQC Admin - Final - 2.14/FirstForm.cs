﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IQCAdmin
{
    public partial class FirstForm : Form
    {
        public FirstForm()
        {
            InitializeComponent();
        }
        List<string> listPartno = new List<string>();
        List<string> newlistPartno = new List<string>();
        private void FirstForm_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            comboBox1.AutoCompleteCustomSource.Clear();
            comboBox1.Items.AddRange(DBHelper.ComboBoxItems("inspectiondata WHERE inspection_type <> 'LABEL AND QUANTITY'", "invoice_no",0).ToArray());
            comboBox1.AutoCompleteCustomSource.AddRange(DBHelper.ComboBoxItems("inspectiondata WHERE inspection_type <> 'LABEL AND QUANTITY'", "invoice_no", 0).ToArray());
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            listPartno.Clear();
            comboBox2.AutoCompleteCustomSource.Clear();
            comboBox2.Text = "";
            listPartno.AddRange(DBHelper.ComboBoxItems("LotNumber WHERE invoice_no = '" + comboBox1.Text + "'", "MaterialCodeBoxSeqID", 0).ToArray());
            comboBox2.Items.AddRange(DBHelper.ComboBoxItems("LotNumber WHERE invoice_no = '" + comboBox1.Text + "'", "MaterialCodeBoxSeqID", 0).ToArray());
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'IQCDatabaseDataSet.SP_SummaryReport_Functional' table. You can move, or remove it, as needed.
            this.SP_SummaryReport_FunctionalTableAdapter.Fill(this.IQCDatabaseDataSet.SP_SummaryReport_Functional, comboBox1.Text, comboBox2.Text);
            // TODO: This line of code loads data into the 'IQCDatabaseDataSet.SP_SummaryReport_Dimensional' table. You can move, or remove it, as needed.
            this.SP_SummaryReport_DimensionalTableAdapter.Fill(this.IQCDatabaseDataSet.SP_SummaryReport_Dimensional, comboBox1.Text, comboBox2.Text);
            // TODO: This line of code loads data into the 'IQCDatabaseDataSet.SP_SummaryReport_Appearance' table. You can move, or remove it, as needed.
            this.SP_SummaryReport_AppearanceTableAdapter.Fill(this.IQCDatabaseDataSet.SP_SummaryReport_Appearance, comboBox1.Text, comboBox2.Text);
            // TODO: This line of code loads data into the 'IQCDatabaseDataSet.SP_SummaryReport_Inspection' table. You can move, or remove it, as needed.
            this.SP_SummaryReport_InspectionTableAdapter.Fill(this.IQCDatabaseDataSet.SP_SummaryReport_Inspection, comboBox1.Text, comboBox2.Text);
            // TODO: This line of code loads data into the 'IQCDatabaseDataSet.SP_SummaryReport_SampleSize' table. You can move, or remove it, as needed.
            this.SP_SummaryReport_SampleSizeTableAdapter.Fill(this.IQCDatabaseDataSet.SP_SummaryReport_SampleSize, comboBox1.Text, comboBox2.Text);

            var uri = new System.Uri("C:\\IQC Admin (Resources)\\default.png").AbsoluteUri;
            ReportParameter img = new ReportParameter("img_judgement", uri);
            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { img });

            this.reportViewer1.RefreshReport();
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FirstForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                var finalJudgement = MessageBox.Show("What is your overall judgement for this report?", "IQC System - Admin", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(finalJudgement == DialogResult.Yes)
                {
                    var uri = new System.Uri("C:\\IQC Admin (Resources)\\passed.png").AbsoluteUri;
                    ReportParameter img = new ReportParameter("img_judgement", uri);
                    this.reportViewer1.LocalReport.EnableExternalImages = true;
                    this.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { img });
                    this.reportViewer1.RefreshReport();

                    DBHelper.overallJudgement(comboBox1.Text, comboBox2.Text, "PASSED");
                }
                else
                {
                    var uri = new System.Uri("C:\\IQC Admin (Resources)\\failed.png").AbsoluteUri;
                    ReportParameter img = new ReportParameter("img_judgement", uri);
                    this.reportViewer1.LocalReport.EnableExternalImages = true;
                    this.reportViewer1.LocalReport.SetParameters(new ReportParameter[] { img });
                    this.reportViewer1.RefreshReport();

                    DBHelper.overallJudgement(comboBox1.Text, comboBox2.Text, "FAILED");
                }
            }
        }

        private void comboBox2_TextChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            newlistPartno.Clear();
            foreach (var item in listPartno)
            {
                if (item.Contains(comboBox2.Text))
                {
                    newlistPartno.Add(item);
                }
            }
            comboBox2.Items.AddRange(newlistPartno.ToArray());
            comboBox2.SelectionStart = comboBox2.Text.Length;
            Cursor = Cursors.Default;
            comboBox2.DroppedDown = true;
        }
    }
}
