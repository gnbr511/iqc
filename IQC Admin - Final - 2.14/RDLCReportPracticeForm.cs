﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;
using System.Data.SqlClient;

namespace IQCAdmin
{
    public partial class RDLCReportPracticeForm : Form
    {
        public RDLCReportPracticeForm()
        {
            InitializeComponent();
        }

        private void RDLCReportPracticeForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.ReportPath = "Report1.rdlc";
            ReportDataSource rds = new ReportDataSource("RDLCReportPracticeDataSet", GetLotNumber());
            this.reportViewer1.LocalReport.DataSources.Add(rds);
            this.reportViewer1.RefreshReport();
        }
        private List<RDLCReportPractice> GetLotNumber()
        {
            List <RDLCReportPractice> list = new List<RDLCReportPractice>();
            SqlCommand command = new SqlCommand("SELECT * FROM LotNumber WHERE invoice_no = 'NCV02833'", DBHelper.connection);
            var reader = command.ExecuteReader();
            while (reader.Read())
            {
                RDLCReportPractice rDLCReportPractice = new RDLCReportPractice 
                { 
                    id = (int)reader["id"], 
                    invoiceNo = (string)reader["invoice_no"],
                    partNO = (string)reader["part_no"],
                    MaterialCodeBoxSeqId = (string)reader["MaterialCodeBoxSeqId"]
                };
                list.Add(rDLCReportPractice);
            }
            return list;
        }
        

    }
}
